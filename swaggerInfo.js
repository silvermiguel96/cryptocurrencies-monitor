const swaggerJSDoc = require("swagger-jsdoc");

const swaggerDefinition = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "API CRYPTO",
      version: "0.1.0",
      description:
        "This is a simple CRUD API application made with Express and documented with Swagger",
      license: {
        name: "MIT",
        url: "https://spdx.org/licenses/MIT.html",
      },
      contact: {
        name: "miguelangel",
        email: "silvermiguel96@gmail.com",
      },
    },
    servers: [
      {
        url: "http://localhost:3000/",
      },
    ],
  },
};

// Configuracion de swaggerUi
const options = {
  swaggerDefinition,
  // Lectura de todas las rutas
  apis: ["./api/routes/*.js"],
};

const specs = swaggerJSDoc(options);

module.exports = specs;
