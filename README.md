# API de WChallenge - Cryptocurrencies Monitor

## Deploy

$ npm install

$ cp .env.example .env

## Testing

$ npm test

## Uso

$ npm start

## Documentacion en la ruta /api-docs

## Pruebas

Ruta para pruebas https://cypto.herokuapp.com/
