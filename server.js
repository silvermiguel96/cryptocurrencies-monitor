require("dotenv").config();
const express = require("express");
const app = express();
const port = process.env.PORT || 3000;
const host = process.env.HOST || "127.0.0.1";
const mongoose = require("./config/database/config");
const bodyParser = require("body-parser");
const swaggerUi = require("swagger-ui-express");
const swaggerInfo = require("./swaggerInfo");
const jwt = require("jsonwebtoken");
app.set("secretKey", process.env.JWT || "secretClave");

const dbConnection = mongoose.connection;
dbConnection.on("error", (err) => console.log(`Connection error ${err}`));
dbConnection.once("open", () => console.log("Connected to DB!"));

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerInfo));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

function private(req, res, next) {
  let token = "";
  if (req.headers.authorization) {
    token = req.headers.authorization.substring(7);
  }
  jwt.verify(token, req.app.get("secretKey"), function (err, decoded) {
    if (err) {
      res.json({ status: 403, message: err.message, data: null });
    } else {
      req.body.user = decoded.id;
      req.body.userMoney = decoded.typeMoney;
      next();
    }
  });
}

const userPublicRoutes = require("./api/routes/userPublicRoutes");
const cryptoPrivateRoutes = require("./api/routes/cryptoPrivateRoutes");

app.use("/users", userPublicRoutes);
app.use("/cryptos", private, cryptoPrivateRoutes);

app.use(function (req, res, next) {
  let err = new Error("Not Found");
  err.status = 404;
  next(err);
});

app.use(function (err, req, res, next) {
  console.error(err);
  if (err.status === 404) res.status(404).json({ message: "Not found" });
  else res.status(500).json({ message: "Error interno en el servidor!!" });
});

app.listen(port, () => {
  console.log(`Api up in http://${host}:${port}`);
});
