const { cryptoMoneyValidationRules, validate } = require('../validator/cryptoMoneyValidationRules.js');
const express = require("express");
const router = express.Router();
const crypyoController = require("../controllers/CrypyoController");

/**
 @swagger
 * /cryptos/add:
 *   post:
 *    security:
 *	   - jwt: []
 *    summary: Registar una crpto moneda.
 *    description: Registar una crpto moneda.
 *    tags: [cryptos]
 *    parameters:
 *       - in: body
 *         name: coinId
 *         required: true
 *         description: id de la crypto moneda.
 *         schema:
 *           type: string
 *    responses:
 *      200:
 *        description: Usuario agregado exitosamente.
 *        content:
 *          application/json:
 */
router.post("/add", cryptoMoneyValidationRules('addCrypto'), validate, crypyoController.addCrypto);

/**
 @swagger
 * /cryptos/list:
 *   post:
 *    security:
 *	   - jwt: []
 *    summary: Listar las crypto monedas.
 *    description: Listar las crypto monedas.
 *    tags: [cryptos]
 *    responses:
 *      200:
 *        description: Lista de criptomonedas exitosa.
 *        content:
 *          application/json:
 */
router.get("/list", crypyoController.listCrypto);

/**
 @swagger
 * /cryptos/list/coins:
 *   post:
 *    security:
 *	   - jwt: []
 *    summary: Listar las crypto monedas por numero top.
 *    description: Listar las crypto monedas por numero top.
 *    tags: [cryptos]
 *    parameters:
 *       - in: query
 *         name: numberTop
 *         required: true
 *         description: numero de busqueda.
 *         schema:
 *           type: string
 *       - in: query
 *         name: orden
 *         required: true
 *         description: busqueda tipo de orden (ascendente/descendente).
 *         schema:
 *           type: string
 *    responses:
 *      200:
 *        description: Lista de criptomonedas exitosa.
 *        content:
 *          application/json:
 */
router.get("/coins", cryptoMoneyValidationRules('listCryptoTop'), validate, crypyoController.listCryptoTop);

module.exports = router;
