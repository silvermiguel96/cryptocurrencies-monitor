const { userValidationRules, validate } = require('../validator/userValidator.js');
const express = require("express");
const router = express.Router();
const userController = require("../controllers/UserControllers");

/**
 @swagger
 * /users/register:
 *   post:
 *    summary: Registra un usuario.
 *    description: Registra un usuario .
 *    tags: [Users]
 *    parameters:
 *       - in: body
 *         name: name
 *         required: true
 *         description: Nombre del usuario.
 *         schema:
 *           type: string
 *       - in: body
 *         name: lastname
 *         required: true
 *         description: Apellido del usuario.
 *         schema:
 *           type: string
 *       - in: body
 *         name: username
 *         required: true
 *         description: Apodo del usuario.
 *         schema:
 *           type: string
 *       - in: body
 *         name: password
 *         required: true
 *         description: Contraseña del usuario.
 *         schema:
 *           type: string
 *       - in: body
 *         name: typeMoney
 *         required: true
 *         description: Tipo de mondea.
 *         schema:
 *           type: string
 *    responses:
 *      200:
 *        description: Usuario agregado exitosamente.
 *        content:
 *          application/json:
 */
router.post("/register", userValidationRules('createUser'), validate, userController.create);

/**
 @swagger
 * /users/login:
 *   post:
 *    summary: Login usuario.
 *    description: Login usuario .
 *    tags: [Users]
 *    parameters:
 *       - in: body
 *         name: username
 *         required: true
 *         description: Apodo del usuario.
 *         schema:
 *           type: string
 *       - in: body
 *         name: password
 *         required: true
 *         description: Contraseña del usuario.
 *         schema:
 *           type: string
 *    responses:
 *      200:
 *        description: El usuario ha sido autenticado.
 *        content:
 *          application/json:
 */
router.post("/login", userValidationRules('loginUser'), validate, userController.authenticate);

module.exports = router;
