const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const TYPEMONEY_EMUN = ["eur", "usd", "ars"];

const saltRounds = 6; 

const Schema = mongoose.Schema;
const UserSchema = new Schema({
  name: {
    type: String,
    trim: true,
    required: true,
  },
  lastname: {
    type: String,
    trim: true,
    required: true,
  },
  username: {
    type: String,
    trim: true,
    unique: true,
    required: true,
  },
  password: {
    type: String,
    trim: true,
    required: true,
  },
  typeMoney: {
    type: String,
    trim: true,
    required: true,
    enum: TYPEMONEY_EMUN,
  },
});

UserSchema.pre("save", function (next) {
  this.password = bcrypt.hashSync(this.password, saltRounds);
  next();
});

module.exports = mongoose.model("User", UserSchema);
