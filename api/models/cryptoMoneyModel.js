const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const CryptoMoneySchema = new Schema({
  cryptoId: {
    type: String,
    trim: true,
    required: true,
  },
  symbol: {
    type: String,
    trim: true,
    required: true,
  },
  current_price: {
    type: Number,
    trim: true,
    required: true,
  },
  name: {
    type: String,
    trim: true,
    required: true,
  },
  image: {
    type: String,
    trim: true,
    required: true,
  },
  last_updated: {
    type: Date,
    trim: true,
    required: true,
  },
  user: {
    type: String,
    trim: true,
    required: true,
  },
});

module.exports = mongoose.model("CryptoMoney", CryptoMoneySchema);
