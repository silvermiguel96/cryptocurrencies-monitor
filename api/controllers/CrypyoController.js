const cryptoMoney = require("../models/cryptoMoneyModel");
const cryptoMoneyServices = require("../services/cryptoMoney");

module.exports = {
  addCrypto: async function (req, res, next) {
    const { userMoney, user, coinId } = req.body;
    const [crypto] = await cryptoMoneyServices.addCrypto(userMoney, coinId);
    if (!crypto) {
      res.json({
        status: 404,
        message: `No se encontró moneda con él, id ${coinId}`,
        data: null,
      });
      return;
    }
    const { id, symbol, current_price, name, image, last_updated } = crypto;
    const coins = await cryptoMoney.findOne({ name, user }).exec();
    if (coins) {
      res.json({
        status: 403,
        message: "El usuario ya posee la moneda!",
        data: null,
      });
      return;
    }

    cryptoMoney.create(
      { cryptoId: id, symbol, current_price, name, image, last_updated, user },
      function (err, coin) {
        if (err) {
          res.json({
            status: 400,
            message: err.message,
            data: null,
          });
        } else
          res.json({
            status: 200,
            message: "La moneda ha sido agrega al usuario exitosamente.",
            data: coin,
          });
      }
    );
  },

  listCryptoTop: async function (req, res, next) {
    try {
      let filterOrder = -1;
      const { user } = req.body;
      const { numberTop, orden } = req.query;
      if (orden == "ascendente") {
        filterOrder = 1;
      }

      const coins = await cryptoMoney
        .find({ user })
        .sort({ current_price: filterOrder })
        .limit(parseInt(numberTop));

      const coinsPrices = await Promise.all(
        coins.map(async (coin) => {
          const prices = await cryptoMoneyServices.listCryptoTop(coin.cryptoId);
          const [price] = Object.values(prices);
          return {
            symbol: coin.symbol,
            current_price_argentine_peso: price.ars,
            current_price_dollar: price.usd,
            current_price_euro: price.ars,
            name: coin.name,
            image: coin.image,
            last_updated: coin.last_updated,
          };
        })
      );
      res.json({
        status: 200,
        message: "Lista de monedas",
        data: coinsPrices,
      });
    } catch (error) {
      console.error(error);
    }
  },

  listCrypto: async function (req, res, next) {
    try {
      const { userMoney } = req.body;
      const crypto = await cryptoMoneyServices.listCrypto(userMoney);
      const newCryptos = crypto.map((coin) => {
        return {
          symbol: coin.symbol,
          current_price: coin.current_price,
          name: coin.name,
          image: coin.image,
          last_updated: coin.last_updated,
        };
      });
      res.json({
        status: 200,
        message: "Lista de criptomonedas exitosa.",
        data: newCryptos,
      });
    } catch (error) {
      console.error(error);
    }
  },
};
