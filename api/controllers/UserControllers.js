const userModel = require("../models/userModel");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports = {
  create: async function (req, res, next) {
    const { name, lastname, username, password, typeMoney } = req.body;
    const user = await userModel.findOne({ username: username }).exec();

    if (user) {
      res.json({
        status: 400,
        message: "El username ya existe!",
        data: null,
      });
      return;
    }
    userModel.create(
      {
        name,
        lastname,
        username,
        password,
        typeMoney,
      },
      function (err, user) {
        if (err) {
          res.json({
            status: 400,
            message: err.message,
            data: null,
          });
        } else
          res.json({
            status: 200,
            message: "Usuario agregado exitosamente.",
            data: { user },
          });
      }
    );
  },

  authenticate: function (req, res, next) {
    const { username, password } = req.body;
    userModel.findOne({ username: username }, function (err, userInfo) {
      if (err) {
        next(err);
      } else {
        if (userInfo && bcrypt.compareSync(password, userInfo.password)) {
          const token = jwt.sign(
            { id: userInfo._id, typeMoney: userInfo.typeMoney },
            req.app.get("secretKey"),
            { expiresIn: "1h" }
          );
          res.json({
            status: 200,
            message: "El usuario ha sido autenticado!!!",
            data: { user: userInfo, token: token },
          });
        } else {
          res.json({
            status: 400,
            message: "Invalido documento de identidad o contraseña",
            data: null,
          });
        }
      }
    });
  },
};
