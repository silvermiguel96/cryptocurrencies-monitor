global.fetch = require("node-fetch");
const cryptoMoneyServices = {
  async addCrypto(userMoney, coinId) {
    const URL = `${process.env.API_CRYPTO}/v3/coins/markets?vs_currency=${userMoney}&ids=${coinId}`;
    const data = await fetch(URL, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    });
    return await data.json();
  },

  async listCrypto(userMoney) {
    const URL = `${process.env.API_CRYPTO}/v3/coins/markets?vs_currency=${userMoney}`;
    const data = await fetch(URL, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    });
    return await data.json();
  },

  async listCryptoTop(cryptoId) {
    const URL = `${process.env.API_CRYPTO}/v3/simple/price?ids=${cryptoId}%2C&vs_currencies=usd%2Ceur%2Cars`;
    const data = await fetch(URL, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    });
    return await data.json();
  },
};
module.exports = cryptoMoneyServices;
