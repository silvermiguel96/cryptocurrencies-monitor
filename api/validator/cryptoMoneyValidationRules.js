const CARACTERES = /^[a-zA-Z0-9]+$/;
const { body, query, validationResult } = require("express-validator");
const cryptoMoneyValidationRules = (method) => {
  switch (method) {
    case "addCrypto": {
      return [
        body("coinId", "coin id no existe").exists()
      ];
    }
    case "listCryptoTop": {
      return [
        query("numberTop", "Usuario no existe").exists(),
        query("numberTop", "NumeroTop menor o igual a 25").isFloat({min:1, max: 25}),
      ];
    }
  }
};

const validate = (req, res, next) => {
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    return next();
  }
  const extractedErrors = [];
  errors.array().map((err) => extractedErrors.push({ [err.param]: err.msg }));

  return res.status(422).json({
    errors: extractedErrors,
  });
};

module.exports = {
  cryptoMoneyValidationRules,
  validate,
};
