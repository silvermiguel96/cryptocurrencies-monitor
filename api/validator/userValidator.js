const CARACTERES = /^[a-zA-Z0-9]+$/;
const { body, validationResult } = require("express-validator");
const userValidationRules = (method) => {
  switch (method) {
    case "createUser": {
      return [
        body("name", "Name no existe").exists(),
        body("lastname", "Apellido no existe").exists(),
        body("username", "Usuario no existe").exists(),
        body("password", "Contraseña no existe").exists(),
        body("password", "Contraseña mayor a 8 caracteres").isLength({
          min: 8,
        }),
        body("password", "Contraseña Alfanumerica").custom((value) =>
          CARACTERES.test(value)
        ),
        body("typeMoney").optional().isIn(["eur", "usd", "ars"]),
      ];
    }
    case "loginUser": {
      return [
        body("username", "Usuario no existe").exists(),
        body("password", "Contraseña no existe").exists(),
      ];
    }
  }
};

const validate = (req, res, next) => {
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    return next();
  }
  const extractedErrors = [];
  errors.array().map((err) => extractedErrors.push({ [err.param]: err.msg }));

  return res.status(422).json({
    errors: extractedErrors,
  });
};

module.exports = {
  userValidationRules,
  validate,
};
