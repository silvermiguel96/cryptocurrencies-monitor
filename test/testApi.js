let chai = require("chai");
let chaiHttp = require("chai-http");
const expect = require("chai").expect;
chai.use(chaiHttp);
const port = process.env.PORT || 3000;
const host = process.env.HOST || "127.0.0.1";
const url = `http://${host}:${port}`;

describe("Usuarios ", async () => {
  await it("Registrar un usuario", (done) => {
    chai
      .request(url)
      .post("/users/register")
      .send({
        name: "prueba1",
        lastname: "prueba last name",
        username: "pruebaUser",
        password: "qweqweqwe",
        typeMoney: "eur",
      })
      .end(function (err, res) {
        expect(res).to.have.status(200);
        done();
      });
  });
});

describe("Login ", () => {
  it("Consultando autenticacion", (done) => {
    chai
      .request(url)
      .post("/users/login")
      .send({
        username: "silvermiguel96",
        password: "qweqweqwe",
      })
      .end(function (err, res) {
        expect(res).to.have.status(200);
        done();
      });
  });
});

describe("Listar Crypto monedas", () => {
  it("Listar todas las crypto monedas", (done) => {
    chai
      .request(url)
      .get("/cryptos/list")
      .end(function (err, res) {
        expect(res).to.have.status(200);
        done();
      });
  });
});

describe("Agregar Crypto Moneda", () => {
  it("Agregar Crypto Moneda", (done) => {
    chai
      .request(url)
      .post("/cryptos/coins")
      .send({
        coinId: "libra-2",
      })
      .end(function (err, res) {
        expect(res).to.have.status(200);
        done();
      });
  });
});

describe("Listar Top Crypto Monedas", () => {
  it("Listar monedas con N Top", (done) => {
    chai
      .request(url)
      .post("/cryptos/coins")
      .send({
        numberTop: "2",
        orden: "descendente",
      })
      .end(function (err, res) {
        expect(res).to.have.status(200);
        done();
      });
  });
});
