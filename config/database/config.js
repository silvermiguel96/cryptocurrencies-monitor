const mongoose = require("mongoose");

const mongoDB = process.env.MONGODB__URL;
mongoose.connect(
  mongoDB,
  { useNewUrlParser: true, useUnifiedTopology: true },
);
mongoose.Promise = global.Promise;
module.exports = mongoose;
